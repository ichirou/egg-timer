//
//  ViewController.swift
//  Egg Timer
//
//  Created by Richmond Ko on 31/08/2016.
//  Copyright © 2016 Richmond Ko. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var timer = Timer()
    var currentTime = 210
    
    @IBOutlet var timerDisplay: UILabel!
    
    @IBAction func reset(_ sender: AnyObject) {
        timer.invalidate()
        currentTime = 210
        timerDisplay.text = String(currentTime)
    }
    
    @IBAction func plusTen(_ sender: AnyObject) {
        currentTime += 10
        timerDisplay.text = String(currentTime)
    }
    
    @IBAction func minusTen(_ sender: AnyObject) {
        currentTime -= 10
        if (currentTime < 0) {
            currentTime = 0
            timer.invalidate()
        }
        timerDisplay.text = String(currentTime)
    }
    
    @IBAction func pause(_ sender: AnyObject) {
        print("pause")
        timer.invalidate()
    }
    
    @IBAction func startTimer(_ sender: AnyObject) {
        print("start")
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ViewController.processTimer), userInfo: nil, repeats: true)
    }
    
    func processTimer() {
        
        if(currentTime != 0) {
            currentTime -= 1
            timerDisplay.text = String(currentTime)
        }
        else {
            timer.invalidate()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

